package com.check.demo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * @Package: com.check.demo
 * @ClassName: test1
 * @Author: 64405
 * @CreateTime: 2021/9/2 21:36
 * @Description:
 */
public class test1 {
    public static void main(String[] args) throws IOException {
        //创建Socket 绑定8080端口，设置最大链接队列为10
        ServerSocket serverSocket = new ServerSocket(80,10 );
        //开始监听
        while (true){
            Socket socket = serverSocket.accept();
            //获取输入流
            InputStream is = socket.getInputStream();
            //读取字节
            int i;
            int j = 0 ;
            byte[] arr = new byte[2048];
            is.read(arr);
            //创建输出流
            OutputStream os =  socket.getOutputStream();
            //=====开始对请求头进行分析========
            //剥离Get请求
            String https = new String(arr);
            String httpHeader = https.substring(https.indexOf("GET") +3 ,https.indexOf("HTTP"));
            System.out.println(httpHeader);
            System.out.println(https);
            //加法运算
            if(httpHeader.indexOf("/add?") > 0){
                //GET 地址正确，剥离参数
                int a = Integer.parseInt(httpHeader.substring(httpHeader.indexOf("a=")+2,httpHeader.indexOf("&")));
                int b = Integer.parseInt(httpHeader.substring(httpHeader.indexOf("b=")+2).trim());
                String m = "<h1>" + (a+b)+ "</h1>";
                os.write(m.getBytes());
            //乘积运算
            }else if(httpHeader.indexOf("/mult?") > 0){
                int a = Integer.parseInt(httpHeader.substring(httpHeader.indexOf("a=")+2,httpHeader.indexOf("&")));
                int b = Integer.parseInt(httpHeader.substring(httpHeader.indexOf("b=")+2).trim());
                String m = "<h1>" + (a*b)+ "</h1>";
                os.write(m.getBytes());
            }
            else {
                os.write("HTTP/1.1 404 OK\r\n".getBytes());
                os.write("Content-Length:38\r\n".getBytes());
                os.write("Server:gybs\r\n".getBytes());
                os.write(("Date:"+new Date()+"\r\n").getBytes());
                os.write("\r\n".getBytes());
                os.write("<h1>ERRO !</h1>".getBytes());
                os.write("<h3>非法参数，404! </h3>".getBytes("utf-8"));
            }
            os.close();
            is.close();
            socket.close();
        }
    }
}
